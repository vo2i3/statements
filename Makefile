ARTICLE=statements
$(ARTICLE).pdf: $(ARTICLE).tex $(ARTICLE).bib
	pdflatex $(ARTICLE).tex
#	biber $(ARTICLE)	
#	pdflatex $(ARTICLE).tex

all: $(ARTICLE).pdf

view:
	okular $(ARTICLE).pdf &

zip:
	zip $(ARTICLE) $(ARTICLE).tex \
	 	$(ARTICLE).bib \
		$(ARTICLE).pdf \
		Makefile \
	  -r images


clean:
	rm -fv $(ARTICLE).aux $(ARTICLE).log $(ARTICLE).out \
		$(ARTICLE).pdf *.bcf *.xml *.bbl *.blg *.zip
